# CloudX Panel

Work in Progress

## Architecture Diagram

Work in Progress

## Tech Stacks

- [Nuxt.js](https://nuxtjs.org)
- [Vue.js](https://vuejs.org)
- [Axios](https://github.com/axios/axios)
- [Vuex](https://vuex.vuejs.org)

## Requirements

- NVM v12.16.1 (NodeJS v12.16.1)
- Yarn 1.22.1

## Development Guide

- Built with Vue.js shipped using Nuxt.js which uses Yarn as its package manager. For detailed explanation on how things work, please check out the following documentations:

  - [Install NVM on Mac](https://medium.com/@isaacjoe/best-way-to-install-and-use-nvm-on-mac-e3a3f6bc494d)
  - [Install NVM on Ubuntu/WSL](https://www.digitalocean.com/community/tutorials/how-to-install-node-js-on-ubuntu-16-04)
  - [Nuxt.js Documentation](https://nuxtjs.org)
  - [Vue.js Documentation](https://vuejs.org)
  - [Nuxt Server](https://blog.lichter.io/posts/nuxt-with-an-api/)

- Please try to avoid any other JS framework such as jQuery.

- Please don't use any other package manager such as NPM. Only use Yarn.

- Don't push directly to Master, always use Pull Request to push updates to Master.

- All variables should be written in camelCase format, using 2 spaces, and include 1 new blank line at the end of each file.

- Take Performance & Security seriously.

- Performance should always be your main priority. This includes your decision to add a new package. Keep the bundled size as small as possible.

- Always prepare for the worst scenario for each actions (exceptions / error handling), as in the app should never display a non-friendly error message if one of the response/request is failed.

- Asset files (JS, CSS, and Images) should be minifed/compressed on production environment.

- Always ask for a review.

## Coding Standard

### CSS

- Use `-` for separator.
- Use `--` for variant.
- Use `__` for subcomponent.

## Installation for Local Development Phase

> Important Notes: All tests must be done using Production build.

- Development Build (support hot reload)

  ```sh
  # Copy the configuration file
  $ cp .env.local.sample .env

  # Install the dependencies
  $ yarn install

  # Start the dev server
  $ yarn dev
  ```

- Production Build

  ```sh
  # Copy the configuration file
  $ cp .env.local.sample .env

  # Install the dependencies
  $ yarn install

  # Build the project
  $ yarn build

  # Start the server
  $ yarn start
  ```

Once the processes are completed, you can go to https://localhost:3000 in your browser.

## Installation for Production Phase

> For these servers, it is required to use Production build.

### Automatic Installation (Install & Uninstall)

- Production

  ```sh
  # To build, and deploy it right away
  make build deploy

  # To only build
  make build

  # To only deploy
  make deploy
  ```

- Development

  ```sh
  # To build, and deploy it right away
  ENV=development make build deploy

  # To only build
  ENV=development make build

  # To only deploy
  ENV=development make deploy
  ```

- Local

  ```sh
  # To build, and deploy it right away
  ENV=local make build deploy

  # To only build
  ENV=local make build

  # To only deploy
  ENV=local make deploy
  ```

### Manual Installation

- Production

  ```sh
  $ docker build . -f ./deploy/web/Dockerfile -t cloudxpanel-production --build-arg ENV=production

  $ docker run --rm -d -p 8080:3000 --name=cloudxpanel-production-container cloudxpanel-production
  ```

- Development

  ```sh
  $ docker build . -f ./deploy/web/Dockerfile -t cloudxpanel-development --build-arg ENV=development

  $ docker run --rm -d -p 8080:3000 --name=cloudxpanel-development-container cloudxpanel-development
  ```

- Local

  ```sh
  $ docker build . -f ./deploy/web/Dockerfile -t cloudxpanel-local --build-arg ENV=local

  $ docker run --rm -d -p 8080:3000 --name=cloudxpanel-local-container cloudxpanel-local
  ```

### Manual Uninstall

To remove an existing image, you can use the following command:

```sh
$ docker image rm <image_tag> -f
```

To stop an running container, you can use the following command:

```sh
$ docker stop <container_id>
```

To get in to the container, you can use the following command:

```sh
$ docker exec -it <container_id> sh
```

## Linter

This project comes with linter for JS and CSS. To run the linter, simply run the following command:

```sh
# JS Lint
$ yarn lint

# JS Lint with auto fix enabled
$ yarn lint --fix

# CSS Lint
$ yarn lintcss

# CSS Lint with auto fix enabled
$ yarn lintcss --fix
```

## Unit Testing

```sh
$ yarn test
```

## JSON Generator/Updater

On this project, some of the pages are using static data, such as List of Countries, Country's Phone Code, Country's States, and State's Cities.

If there is an update to these lists you can simply get the updated .json file by running the following command:

```sh
# Only if you haven't install the dependencies for the generator.
$ cd generator && yarn install && cd ../

# Once you have the generator's dependencies intalled, run the following command in the root directory.
$ yarn update-json
```

Once completed, you should copy-paste the .json files inside `generator` directory into `./static/json` .
