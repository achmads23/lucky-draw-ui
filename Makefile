.PHONY: build deploy

# User Configureable
export ENV								?= production
export PORT_APP						?= 8080

# Static
SERVICE_NAME							= cloudxpanel
DOCKER_FILE								= ./deploy/web/Dockerfile
PORT_FORWARD							= 3000
IMAGE_NAME								= $(SERVICE_NAME)-$(ENV)
CONTAINER_NAME						= $(IMAGE_NAME)-container

build:
	docker stop $(CONTAINER_NAME) || true
	docker image rm $(IMAGE_NAME) -f || true
	docker build . -f $(DOCKER_FILE) -t $(IMAGE_NAME) --build-arg ENV=$(ENV)

deploy:
	docker run --rm -d -p $(PORT_APP):$(PORT_FORWARD) --name=$(CONTAINER_NAME) $(IMAGE_NAME)

stop:
	docker stop $(CONTAINER_NAME)
