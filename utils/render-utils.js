function r(page) {
  return async () => {
    const result = await page();
    return result.default || result;
  };
}

export default r;
