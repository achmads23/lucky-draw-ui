module.exports = {
  // add your custom config here
  // https://stylelint.io/user-guide/configuration
  extends: 'stylelint-config-standard',
  rules: {
    'selector-list-comma-newline-after': null,
    'no-descending-specificity': null,
    'value-list-comma-newline-after': null,
    'declaration-colon-newline-after': null,
    'color-hex-case': 'lower',
    'font-family-no-missing-generic-family-keyword': null,
    'no-duplicate-selectors': null,
    'block-closing-brace-newline-after': null,
    'at-rule-empty-line-before': null,
    'at-rule-no-unknown': [true, { ignoreAtRules: ['mixin', 'function', 'include', 'each', 'for', 'if', 'else', 'extend', 'while', 'return', 'warn'] }],
  },
  ignoreFiles: ['coverage/**', 'assets/css/legacy/custom.css', 'assets/css/legacy/sb-admin-2.css', 'static/**'],
};
