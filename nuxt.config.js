import head from './config/head';
import css from './config/css';
import plugins from './config/plugins';
import buildModules from './config/buildModules';
import modules from './config/modules';
import axios from './config/axios';
import proxy from './config/proxy';
import build from './config/build';
import env from './config/env';
import server from './config/server';

module.exports = {
  mode: 'spa',
  server,
  env,
  head,
  loading: { color: '#fff' },
  css,
  plugins,
  buildModules,
  modules,
  axios,
  proxy,
  build,
  generate: {
    routes: ['/'],
  },
};
