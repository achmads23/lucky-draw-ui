/**
 * @module helpers/table
 */

export function getSummaryStart(limit = 1, page = 1, totalData = 1) {
  if (totalData === 0) return 0;
  return parseInt(limit, 10) * parseInt(page, 10) - parseInt(limit, 10) + 1;
}

export function getSummaryEnd(limit = 1, totalData = 1, itemStart = 1) {
  return Math.min(parseInt(itemStart, 10) + parseInt(limit, 10) - 1, parseInt(totalData, 10));
}

export function getSummaryTotalPage(limit = 1, totalData = 1) {
  const td = parseInt(totalData, 10);
  const l = parseInt(limit, 10);
  if (td === 0 && l === 0) {
    return 0;
  }
  return Math.ceil(td / l);
}

export default {
  getSummaryStart,
  getSummaryEnd,
  getSummaryTotalPage,
};
