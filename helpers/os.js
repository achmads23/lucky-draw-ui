/**
 * @module helpers/os
 */

export function getIconOfOs(name = '') {
  const nameLower = name.toLowerCase();
  let icon = 'fas fa-compact-disc other-os';
  if (nameLower.includes('win')) {
    icon = 'fab fa-windows windows-os';
  } else if (nameLower.includes('ubuntu')) {
    icon = 'fab fa-ubuntu ubuntu-os';
  } else if (nameLower.includes('redhat')) {
    icon = 'fab fa-redhat redhat-os';
  } else if (nameLower.includes('fedora')) {
    icon = 'fab fa-fedora fedora-os';
  } else if (nameLower.includes('centos')) {
    icon = 'fab fa-linux linux-os';
  } else if (nameLower.includes('debian') || nameLower.includes('clearos') || nameLower.includes('gentoo')) {
    icon = 'fab fa-ubuntu ubuntu-os';
  }
  return icon;
}

export default {
  getIconOfOs,
};
