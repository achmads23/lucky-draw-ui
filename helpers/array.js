/**
 * @module helpers/array
 */

export const getValues = (arr, index) => {
  if (typeof index !== 'undefined' && index.includes('arr[')) {
    // eslint-disable-next-line no-eval
    return eval(index);
  }
  return typeof arr[index] === 'string' ? arr[index].toLowerCase() : arr[index];
};

export const sortArrayKey = (data, params = null) => {
  const t = params.byComplete ? params.byComplete : params.by;
  data.sort((a, b) => {
    if (params.order === 'asc') {
      if (getValues(a, t) === getValues(b, t)) return 0;
      if (getValues(a, t) > getValues(b, t)) return 1;
      return -1;
    }

    if (getValues(a, t) === getValues(b, t)) return 0;
    if (getValues(a, t) < getValues(b, t)) return 1;
    return -1;
  });
  return data;
};

export const searchArrayKey = (data, columns = [], query = null) => {
  if (query === null || query === '') {
    return data;
  }
  const searchQuery = query.toLowerCase();
  let result = data;
  result = result.filter(item => {
    let found = false;
    for (let i = 0; i < columns.length; i += 1) {
      const column = columns[i];
      const value = item[column];
      if (value.toString().toLowerCase().includes(searchQuery)) {
        found = true;
        break;
      }
    }

    return found;
  });

  return result;
};
export default {
  getValues,
  sortArrayKey,
  searchArrayKey,
};
