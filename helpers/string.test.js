const stringHelper = require('./string');

test('capitalize first letter only', () => {
  expect(stringHelper.capitalize('john doe')).toBe('John doe');
});

test('capitalize first letter of every word', () => {
  expect(stringHelper.capitalizeEachWord('john doe')).toBe('John Doe');
});

test('Convert string from "Camel Case" into "camelCase" format', () => {
  expect(stringHelper.camelCase('Camel Case')).toBe('camelCase');
});
