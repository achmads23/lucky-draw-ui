/**
 * @module helpers/url
 */

export function objectToQueryString(params = {}) {
  const url = Object.keys(params)
    .map(key => `${key}=${params[key]}`)
    .join('&');

  if (url !== '') return `?${url}`;
  return url;
}
export default {
  objectToQueryString,
};
