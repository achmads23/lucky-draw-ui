const urlHelper = require('./url');

test('javascript object to string', () => {
  expect(urlHelper.objectToQueryString({ src: 'nothing', value: 'nothing' })).toBe('?src=nothing&value=nothing');
});
