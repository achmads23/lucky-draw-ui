/**
 * @module helpers/currency
 */

export const currency = [
  { text: 'USD', id: 'usd' },
  { text: 'IDR', id: 'idr' },
];

export default {
  currency,
};
