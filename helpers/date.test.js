const date = require('./date');

test('date conversion', () => {
  expect(date.humanizeTimestamp(1574050520269, '%day%, %dd% %month% %yyyy%', 'EN')).toBe('Monday, 18 November 2019');
});

test('date and time conversion', () => {
  expect(date.humanizeTimestamp(1574050520269, '%day%, %dd% %mmm% %yyyy% @ %hh%:%mi%', 'EN')).toBe('Monday, 18 Nov 2019 @ 11:15');
});

test('date conversion (ID)', () => {
  expect(date.humanizeTimestamp(1574050520269, '%day%, %dd% %month% %yyyy%', 'ID')).toBe('Senin, 18 November 2019');
});

test('date and time conversion (ID)', () => {
  expect(date.humanizeTimestamp(1574050520269, '%day%, %dd% %mmm% %yyyy% pk %hh%:%mi%', 'ID')).toBe('Senin, 18 Nov 2019 pk 11:15');
});

test('prepend zero for number 10', () => {
  expect(date.prependZero(10)).toBe('10');
});

test('prepend zero for number 7', () => {
  expect(date.prependZero(7)).toBe('07');
});

test('prepend zero for number 0', () => {
  expect(date.prependZero(0)).toBe('00');
});

test('time difference in milisecond', () => {
  expect(date.timeDiff(1574050520269, 1574050520279)).toBe(10);
});

test('time difference in second', () => {
  expect(date.timeDiff(1574050520269, 1574050521269, 's')).toBe(1);
});
