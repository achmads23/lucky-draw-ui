import copy from 'copy-to-clipboard';

const copyMixin = {
  methods: {
    copyText(e, val) {
      const text = e.target.innerText;
      if (copy(val)) {
        e.target.innerText = 'Copied!';
        e.target.classList.add('c-copy-link--success');
        setTimeout(() => {
          e.target.innerText = text;
          e.target.classList.remove('c-copy-link--success');
        }, 2000);
      } else {
        console.error(`Failed to copy ${text} to clipboard`);
      }
    },
  },
};

export default copyMixin;
