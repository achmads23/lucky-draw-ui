import VueRecaptcha from 'vue-recaptcha';

const recaptchaMixin = {
  components: { VueRecaptcha },
  data() {
    return {
      recaptchaLoginEnabled: process.env.CLIENT_RECAPTCHA_LOGIN_ENABLED === 'true' || false,
      sitekey: process.env.CLIENT_RECAPTCHA_SITE_KEY || '',
      recaptchaVerified: null,
    };
  },
  created() {
    window.addEventListener('resize', this.initRecaptcha);
  },
  destroyed() {
    window.removeEventListener('resize', this.initRecaptcha);
  },
  mounted() {
    this.initRecaptcha();
  },
  methods: {
    initRecaptcha() {
      const Recaptcha = document.querySelectorAll('div#g-recaptcha > div');
      if (Recaptcha.length > 0) {
        const reCaptchaWidth = Recaptcha[0].offsetWidth;

        const container = document.getElementById('g-recaptcha');
        if (container !== undefined) {
          const containerWidth = container.offsetWidth;
          if (containerWidth < reCaptchaWidth) {
            const captchaScale = containerWidth / reCaptchaWidth;
            container.style.transform = `scale(${captchaScale})`;
          } else {
            container.style.transform = 'scale(1)';
          }
        }
      }
    },
    markRecaptchaAsVerified() {
      this.recaptchaVerified = true;
    },
    resetRecaptcha() {
      this.recaptchaVerified = null;
      this.$refs.recaptcha.reset();
    },
  },
};
export default recaptchaMixin;
