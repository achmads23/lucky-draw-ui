const errorMixin = {
  methods: {
    manageError(err) {
      let message = {};
      if (err === undefined) {
        message = { code: 500, msgs: ['Internal Server Error'] };
      } else if (err.data !== undefined) {
        message = { code: 401, msgs: Array.isArray(err.data.data) ? err.data.data : [err.data] };
      } else {
        message = { code: 401, msgs: [err] };
      }
      return message;
    },
  },
};

export default errorMixin;
