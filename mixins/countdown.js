const countdownMixin = {
  methods: {
    countdown(e, val) {
      return new Promise(resolve => {
        let timeleft = val;
        
        e.innerHTML = timeleft;
        const downloadTimer = setInterval(function() {
          if (timeleft <= 0) {
            clearInterval(downloadTimer);
            resolve(true);
          }
          e.innerHTML = timeleft;
          timeleft -= 1;         
        }, 1000);
      });
    },
  },
};

export default countdownMixin;
