import Auth from './modules/auth-api';
import Type from './modules/type-api';
import Option from './modules/option-api';
import Kegiatan from './modules/kegiatan-api';
import Tahap from './modules/tahap-api';
import Undian from './modules/undian-api';

export default {
  Auth,
  Type,
  Option,
  Kegiatan,
  Tahap,
  Undian,
};
