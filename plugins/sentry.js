import Vue from 'vue';
import * as Sentry from '@sentry/browser';
import * as Integrations from '@sentry/integrations';

const isEnabled = process.env.SENTRY_ENABLED === 'true';
const isDsnConfigured = process.env.SENTRY_DSN !== '';
if (isEnabled && isDsnConfigured) {
  Sentry.init({
    environment: process.env.SERVER_NAME,
    dsn: process.env.SENTRY_DSN,
    integrations: [new Integrations.Vue({ Vue, attachProps: true })],
  });
}
