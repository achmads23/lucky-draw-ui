/* eslint-disable no-unused-vars */
import ApiClient from '~/api';

const initialState = {
  list: [],
};

const getters = {
  getList: state => state.list,
};

const actions = {
  async retrieveList({ commit }, params = null) {
    try {
      const res = await ApiClient.getListType(params);
      if (typeof res.data !== 'undefined') {
        commit('setList', res.data.list);
      } else {
        throw new Error(['Ooops! Internal Server Error']).message;
      }
      return Promise.resolve();
    } catch (e) {
      return Promise.reject(e.response);
    }
  },
  clearList({ commit }) {
    commit('setList', []);
  },
};

const mutations = {
  setList(state, value) {
    state.list = value;
  },
};

export default {
  state: () => ({ ...initialState }),
  getters,
  actions,
  mutations,
};
