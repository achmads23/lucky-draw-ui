/* eslint-disable no-unused-vars */
import ApiClient from '~/api';

const initialState = {
  user: {},
};

const getters = {
  getUser: state => state.user,
};

const actions = {
  retrieveFromCookies({ commit }) {
    commit('setUser', this.$cookies.get('user'));
  },
  setUser({ commit }, params = null) {
    const { user } = params;
    commit('setUser', user);
  },
};

const mutations = {
  setUser(state, value) {
    state.user = value;
  },
};

export default {
  state: () => ({ ...initialState }),
  getters,
  actions,
  mutations,
};
