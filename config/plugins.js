const plugins = [
  'plugins/sentry', 
  'plugins/vue-multiselect', 
  'plugins/tooltip', 
  'plugins/vee-validate',
  'plugins/component-template',
  'plugins/component-directives',
  'plugins/lazy-load',
  'plugins/vue-js-toggle-button',
];

export default plugins;
