require('dotenv').config();

const server = {
  port: process.env.SERVER_PORT || 3000, // default: 3000
  host: process.env.SERVER_HOST || 'localhost', // default: localhost
};

export default server;
