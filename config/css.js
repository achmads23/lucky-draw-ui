const css = ['~/node_modules/vue-multiselect/dist/vue-multiselect.min.css', '~/assets/vendor/font-awesome/css/font-awesome.min.css', '~/assets/vendor/nucleo/css/nucleo.css', '~/assets/vendor/nucleo/css/nucleo-svg.css','~/assets/scss/main.scss'];

export default css;
