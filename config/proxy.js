require('dotenv').config();

const proxy = {
  '/dc': { target: process.env.CLIENT_URL, pathRewrite: { '^/dc/': '' } },
  '/api': { target: process.env.CLIENT_API_URL, pathRewrite: { '^/api/': '' }  }, // Will forward the /api to the target URL
};

export default proxy;
