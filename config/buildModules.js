const buildModules = [
  '@nuxtjs/eslint-module',
  '@nuxtjs/stylelint-module',
  [
    '@nuxtjs/router',
    {
      path: 'router/',
      fileName: 'index.js',
    },
  ],
];

export default buildModules;
