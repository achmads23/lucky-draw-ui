require('dotenv').config();
const { join } = require('path');

const isProduction = process.env.NODE_ENV === 'production';

const build = {
  analyze: false,
  babel: {
    babelrc: true,
  },
  extractCSS: { ignoreOrder: true },
  extend(config, ctx) {
    if (!ctx.isDev) {
      config.devtool = 'source-map';
    }

    // Resolver for all conditions
    const basePath = join(__dirname, '..');
    config.resolve.alias = {
      // TODO: Path alias here for several modules and test files
      ...config.resolve.alias,
      '@': `${basePath}/`,
      tests: `${basePath}/tests/`,
      components: `${basePath}/components/`,
      api: `${basePath}/api/`,
      pages: `${basePath}/pages/`,
      server: `${basePath}/server/`,
      mixins: `${basePath}/mixins/`,
      store: `${basePath}/store/`,
      utils: `${basePath}/utils/`,
    };
  },
  filenames: {
    chunk: isProduction ? '[name]-[contenthash]-bundle.js' : '[name].js',
    app: isProduction ? 'app-[chunkhash]-bundle.js' : '[name].js',
    css: isProduction ? '[contenthash]-bundle.css' : '[name].css',
    img: isProduction ? 'img/[contenthash:7].[ext]' : '[path][name].[ext]',
    font: isProduction ? 'fonts/[contenthash:7].[ext]' : '[path][name].[ext]',
  },
  transpile: ['vee-validate/dist/rules'],
  
};

export default build;
