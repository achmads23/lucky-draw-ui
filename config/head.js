const head = {
  title: process.env.APP_NAME,
  meta: [
    { charset: 'utf-8' },
    { name: 'viewport', content: 'width=device-width, initial-scale=1' },
    {
      hid: 'description',
      name: 'description',
      content: 'An application that reports everything on kayako to you',
    },
  ],
  link: [
    
    { rel: 'icon', type: 'image/x-icon', href: '/img/wof.ico' },
  ],
  script: [{ src: 'https://cdn.jsdelivr.net/npm/feather-icons/dist/feather.min.js' }],
};

export default head;
