require('dotenv').config();

const env = {
  CLIENT_LOGO_COLOR: process.env.CLIENT_LOGO_COLOR || '',
};

export default env;
