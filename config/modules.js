const modules = ['@nuxtjs/axios', '@nuxtjs/dotenv', '@nuxtjs/proxy', 'cookie-universal-nuxt', 'bootstrap-vue/nuxt'];

export default modules;
