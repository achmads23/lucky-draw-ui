/* eslint-disable no-await-in-loop, no-console */
const consola = require('consola');
const axios = require('axios');
const fs = require('fs');

// Configureable Variables
const host = 'http://api-cloudx.local.wowrack.com';
const fileCountryJson = `${__dirname}/list-country.json`;
const fileCountryCodeJson = `${__dirname}/list-country-code.json`;
const fileCountryStateJson = `${__dirname}/list-country-state.json`;
const fileCountryCityJson = `${__dirname}/list-country-city.json`;
const fileCurrencyJson = `${__dirname}/list-currency.json`;

// Don't update anything below
let countries = [];
const states = {};

const writeFile = (fileName, content) => {
  fs.unlink(fileName, err => {
    if (err) {
      // consola.info('File not yet exist');
    }

    fs.appendFile(fileName, JSON.stringify(content), err2 => {
      if (err2) throw err2;
      const { length } = Object.keys(content);
      consola.success(`Fetched ${length} data, and saved to ${fileName}`);
    });
  });
};

const asyncForEach = async (array, callback) => {
  for (let index = 0; index < array.length; index += 1) {
    await callback(array[index], index, array);
  }
};

const fetchCountries = async () => {
  try {
    const url = `${host}/api/get-country-register`;
    consola.info(`Connecting to ${url} ...`);

    const res = await axios.get(url);
    countries = res.data.data;

    writeFile(fileCountryJson, countries);
  } catch (e) {
    consola.error(e);
  }
};

const fetchCountryCode = async () => {
  try {
    const records = {};
    await asyncForEach(countries, async item => {
      const url = `${host}/api/get-phone-code-register/${item.id}`;
      consola.info(`Connecting to ${url} ...`);

      const res = await axios.get(url);
      records[item.id] = [{ text: res.data.data, id: res.data.data }];
    });
    writeFile(fileCountryCodeJson, records);
  } catch (e) {
    consola.error(e);
  }
};

const fetchStates = async () => {
  try {
    const records = {};
    await asyncForEach(countries, async item => {
      const url = `${host}/api/get-states-register/${item.id}`;
      consola.info(`Connecting to ${url} ...`);

      const res = await axios.get(url);
      records[item.id] = res.data.data;
      states[item.id] = res.data.data.map(x => {
        return x;
      });
    });

    writeFile(fileCountryStateJson, records);
  } catch (e) {
    consola.error(e);
  }
};

const fetchCities = async () => {
  try {
    const records = {};
    await asyncForEach(Object.keys(states), async x => {
      await asyncForEach(states[x], async item => {
        const url = `${host}/api/get-cities-register/${item.id}`;
        consola.info(`Connecting to ${url} ...`);

        const res = await axios.get(url);
        if (!records[x]) records[x] = {};
        records[x][item.id] = res.data.data;
      });
    });

    writeFile(fileCountryCityJson, records);
  } catch (e) {
    consola.error(e);
  }
};

const fetchCurrencies = async () => {
  try {
    const url = `${host}/api/currency/list-register`;
    consola.info(`Connecting to ${url} ...`);

    const res = await axios.get(url);
    const currencies = res.data.data;

    writeFile(fileCurrencyJson, currencies);
  } catch (e) {
    consola.error(e);
  }
};

const begin = async () => {
  await fetchCountries();
  await fetchCountryCode();
  await fetchStates();
  await fetchCities();
  await fetchCurrencies();
};

begin();
